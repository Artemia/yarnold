# YARNOLD

Yet Another Arnold is a fork of the Arnold Amstrad CPC Emulator .

YARNOLD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

YARNOLD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The license is as published by the Free Software
Foundation and appearing in the file LICENSE.GPL2
included in the packaging of this software. Please review the following
information to ensure the GNU General Public License requirements will
be met: https://www.gnu.org/licenses/gpl-2.0.html

1. Build Options

	debug has debugging symbols and no optimisations
	release has no symbols and maximum optimisations

	archive.sh - cleans the directory and uses 7-zip to make an archive of the src

	inkz80 versions use inkz80 a Z80 emulation by my friend Mark Incley. This has correct z80 flags and instruction emulation
timing is not correct for cpc at this time.

	arnold's z80 core is the original z80 core with some fixes and correct timings, z80 flags emulation is not perfect.

2. Build on Linux
	1. Prepare Dependencies
	To build YARNOLD emulator, pior need to install some dependencies following you Linux distribution :
		1. Ubuntu and derivates

			On a terminal:

				sudo apt-get install build-essential git cmake pkg-config libwxgtk3.0-dev libsdl2-dev  

		2. Arch Linux and derivates

			On a terminal:

				sudo pacman -S git base-devel cmake wxgtk sdl2

		3. Gentoo / Calculate Linux

			On a terminal as root:

				emerge cmake git wxGTK libsdl2 gtk+

	2. Get a git workcopy
	
		From terminal session:
		
			git clone https://Artemia@bitbucket.org/Artemia/yarnold.git
			cd yarnold
			
	
	3. Final build

		On this git base folder:

			mkdir build && cd build
			cmake -DSDL_VERSION=USE_SDL2 -DZ80_VERSION=USE_INKZ80 -DEMBED_SDL=YES ..
			make -j3

	4. Execute
	
		On Terminal 
		
			./yarnold

3. Build on Windows

	Coming...
