
set(platformSpecific_FILES 
  SDLPlatformSpecific.cpp
  )

include_directories(sdl)
link_libraries( ${SDL_LIBRARY} )

IF(USE_GTK MATCHES "Yes")
    IF(GTK_VERSION MATCHES "USE_GTK3")
	    link_libraries(${GTK3_LIBRARIES})
    ELSE(GTK_VERSION MATCHES "USE_GTK3")
        link_libraries(${GTK2_LIBRARIES})
    ENDIF(GTK_VERSION MATCHES "USE_GTK3")
endif(USE_GTK MATCHES "Yes")

add_library (platformspecific ${platformSpecific_FILES}) 
